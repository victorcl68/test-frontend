# Previdenciarista Dashboard

### O projeto foi desenvolvido com o Linter ESLint e Stylelint configurado nos moldes utilizados pela Trybe (o qual tem como base o Linter Airbnb)

---

## Tecnologias utilizadas
- ReactJS com as seguintes funções e bibliotecas:
    * Context API
    * Hooks
    * React Router DOM
    * JWT Decode
    * React Testing Library
    * Styled Components Ant Design

---

## Para inciar o projeto
* Para rodar localmente o projeto é necessário clonar o repósitorio do modo que preferir

- Para deixar o back-end ativo
    * Certificar que está na pasta raiz do projeto
    * Digitar o comando `json-server --watch db.json -p 8080 -m lmd.js --routes routes.json`
- Para rodar o front-end
    * Acessar a pasta `front-end`
    * Instalar as dependências com o comando `npm install`
    * Iniciar o projeto com o comando `npm start`

---

## Observações
* É necessário ter o Node instalado na maquina para os comandos serem reconhecidos
* O formato para o email deve ser `email@email.com` e a senha maior que 6 caracteres
