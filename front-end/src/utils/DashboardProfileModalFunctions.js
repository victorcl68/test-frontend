export default () => {
  const userDataStringfied = localStorage.getItem('user-data');
  const localStorageParsed = JSON.parse(userDataStringfied);

  const { conta: { email, nome, privilegio } } = localStorageParsed;

  const privilegeCaptalized = privilegio.charAt(0).toUpperCase()
          + privilegio.slice(1).toLowerCase();

  return { nome, email, privilegeCaptalized };
};
