export default (string) => {
  const peticoes = 'Petições';
  const calculos = 'Cálculos';
  const PETICOES = 'PETICOES';
  const CALCULOS = 'CALCULOS';

  const regexUnderescore = /_/g;

  let stringCaptalized = string.charAt(0).toUpperCase()
            + string.slice(1).toLowerCase();

  if (string === PETICOES) return peticoes;
  if (string === CALCULOS) return calculos;

  if (regexUnderescore.test(stringCaptalized)) {
    stringCaptalized = stringCaptalized.replaceAll('_', ' ');
  }

  return stringCaptalized;
};
