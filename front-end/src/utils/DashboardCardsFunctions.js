import axios from 'axios';

const changePosition = (arr, from, to) => {
  const arraySplice = arr.splice(from, 1);
  arr.splice(to, 0, arraySplice[0]);
  return arr;
};

const getModulos = async () => {
  try {
    const userData = localStorage.getItem('user-data');
    const token = localStorage.getItem('token');

    const { conta: { modulos } } = JSON.parse(userData);

    const urlCounterBase = 'http://localhost:8080/counter?tipo=';
    const method = 'get';
    const headers = {
      Authorization: 'token',
      authorization: token,
    };

    const modulosArray = [];

    modulos.forEach((modulo) => modulosArray
      .push(axios({ url: `${urlCounterBase}${modulo}`, method, headers })));

    changePosition(modulosArray, 2, 1);

    return Promise.all(modulosArray);
  } catch (error) {
    console.log(error);
  }
};

export { getModulos, changePosition };
