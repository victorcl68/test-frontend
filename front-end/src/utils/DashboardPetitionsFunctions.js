import axios from 'axios';

export default async (numberPagination = 1) => {
  try {
    const token = localStorage.getItem('token');
    const UrlPetitionsBase = `http://localhost:8080/peticoes?_page=${numberPagination}&_limit=2`;
    const method = 'get';
    const headers = {
      Authorization: 'token',
      authorization: token,
    };

    return axios({ url: `${UrlPetitionsBase}`, method, headers })
      .then((response) => response);
  } catch (error) {
    console.log(error);
  }
};
