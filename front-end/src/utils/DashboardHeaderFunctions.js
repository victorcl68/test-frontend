export default () => {
  const userData = localStorage.getItem('user-data');
  const { conta: { urlImagemPerfil } } = JSON.parse(userData);
  return urlImagemPerfil;
};
