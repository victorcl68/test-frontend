import axios from 'axios';
import jwtDecode from 'jwt-decode';

const requestToken = async () => {
  try {
    const url = 'http://localhost:8080/oauth';
    const requestData = { username: 'teste', password: 'frontend' };

    const { data: { access_token: acessToken } } = await axios
      .post(url, requestData);

    return acessToken;
  } catch (error) {
    console.log(error);
  }
};

const decodeJwt = async (acessToken) => {
  try {
    const accessTokenResolved = await acessToken;
    const responseDecoded = jwtDecode(accessTokenResolved);
    const responseDecodedStringfied = JSON.stringify(responseDecoded);

    return { responseDecodedStringfied, accessTokenResolved };
  } catch (error) {
    console.log(error);
  }
};

const setTokenInLocalStorage = async (jwtTokenDecoded) => {
  try {
    const { accessTokenResolved, responseDecodedStringfied } = await jwtTokenDecoded;
    localStorage.setItem('token', accessTokenResolved);
    localStorage.setItem('user-data', responseDecodedStringfied);
  } catch (error) {
    console.log(error);
  }
};

const validateEmail = (email) => {
  const emailRegex = /^([\w-]+\.)*[\w\- ]+@([\w\- ]+\.)+([\w-]{2,3})$/g;
  if (emailRegex.test(email)) return true;
  return false;
};

const validatePassword = (password) => {
  const six = 6;
  if (password.length > six) return true;
  return false;
};

export {
  requestToken,
  decodeJwt,
  setTokenInLocalStorage,
  validateEmail,
  validatePassword };
