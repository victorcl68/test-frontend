/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useEffect, useState } from 'react';

import { useNavigate } from 'react-router-dom';

import { Button, Form, Image, Input, Typography } from 'antd';

import {
  requestToken,
  decodeJwt,
  setTokenInLocalStorage,
  validateEmail,
  validatePassword,
} from '../utils/LoginBoxFunctions';
import previdenciaristaLogo from '../previdenciarista-logo.png';

import 'antd/dist/antd.min.css';
import '../styles/LoginBox.css';

export default function LoginBox() {
  const [isDisabled, setIsDisabled] = useState(true);
  const [emailInput, setEmailInput] = useState('');
  const [passwordInput, setPasswordInput] = useState('');

  const navigate = useNavigate();

  const { Text, Link } = Typography;

  const dashboard = '/dashboard';
  const previdenciaristaLogoClassName = 'previdenciarista-logo-login';
  const loginInputsClassName = 'login-inputs';

  useEffect(() => {
    if (validateEmail(emailInput) && validatePassword(passwordInput)) {
      return setIsDisabled(false);
    }
    return setIsDisabled(true);
  }, [emailInput, passwordInput]);

  const handleChange = ({ target }) => {
    if (target.name === 'email') setEmailInput(target.value);
    if (target.name === 'password') setPasswordInput(target.value);
  };

  const loginActions = () => {
    const token = requestToken();
    const jwtTokenDecoded = decodeJwt(token);
    setTokenInLocalStorage(jwtTokenDecoded)
      .then(() => navigate(dashboard));
  };

  return (
    <main className="login-container">
      <Image
        className={ previdenciaristaLogoClassName }
        preview={ false }
        src={ previdenciaristaLogo }
        alt={ previdenciaristaLogoClassName }
      />
      <Form>
        <label htmlFor="emailOrCpfInput">
          <Text className="label-texts">Email ou CPF</Text>
          <Input
            name="email"
            autoComplete="email@email.com"
            className={ loginInputsClassName }
            placeholder="Digite seu email ou CPF"
            id="emailOrCpfInput"
            onChange={ handleChange }
          />
        </label>
        <label htmlFor="passwordInput">
          <Text className="label-texts">Senha</Text>
          <Input.Password
            name="password"
            autoComplete="Your Password"
            className={ loginInputsClassName }
            placeholder="Digite sua senha"
            onChange={ handleChange }
            id="passwordInput"
          />
        </label>
        <Link className="forget-password" href=" " target="_blank">
          Esqueceu a senha?
        </Link>
        <Button
          className="login-button"
          onClick={ loginActions }
          disabled={ isDisabled }
          danger
          type="submit"
        >
          <Text className="login-button-text">Entrar</Text>
        </Button>
      </Form>
    </main>
  );
}
