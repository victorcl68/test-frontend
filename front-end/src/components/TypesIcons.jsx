import React from 'react';

import {
  CalculatorOutlined, FileTextOutlined, FolderOutlined, UserOutlined,
} from '@ant-design/icons/lib/icons';

import colorsArray from '../utils/DashboardTypeIconsFunctions';

export default () => [
  <UserOutlined
    key="UserOutlined"
    style={ { color: colorsArray()[0] } }
  />,
  <FileTextOutlined
    key="FileTextOutlined"
    style={ { color: colorsArray()[1] } }
  />,
  <CalculatorOutlined
    key="CalculatorOutlined"
    style={ { color: colorsArray()[2] } }
  />,
  <FolderOutlined
    key="FolderOutlined"
    style={ { color: colorsArray()[3] } }
  />,
];
