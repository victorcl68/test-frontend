import React from 'react';

import { Modal } from 'antd';

import getInfoForModal from '../utils/DashboardProfileModalFunctions';

const profileModal = () => {
  const { nome, email, privilegeCaptalized } = getInfoForModal();
  Modal.info({
    title: 'Informações do Usuário',
    content: (
      <article>
        <p>{`Nome: ${nome}`}</p>
        <p>{`Email: ${email}`}</p>
        <p>{`Privilégio: ${privilegeCaptalized}`}</p>
      </article>
    ),
    icon: false,
    okType: 'danger',
    onOk() {},
  });
};

const resumeModal = (resumo) => {
  Modal.info({
    title: 'Informações da Petição',
    content: (
      <article>
        <p>{resumo}</p>
      </article>
    ),
    icon: false,
    okType: 'danger',
    onOk() {},
  });
};

export { profileModal, resumeModal };
