import React, { useContext, useEffect } from 'react';

import { Card, Tag, Typography } from 'antd';

import { EyeOutlined } from '@ant-design/icons/lib/icons';
import { resumeModal } from './Modals';
import getPetitions from '../utils/DashboardPetitionsFunctions';
import captalize from '../utils/DashboardCardsPetitionsFunctions';

import AppContext from '../context/Context';

import 'antd/dist/antd.min.css';
import '../styles/DashboardPetitions.css';

export default function DashboardPetitions() {
  const { petitions, setPetitions } = useContext(AppContext);
  const { Text, Title } = Typography;

  useEffect(() => {
    try {
      const getPetitionsFunction = async () => {
        const { data: petitionsResolved } = await getPetitions();
        setPetitions(petitionsResolved);
      };
      getPetitionsFunction();
    } catch (error) {
      console.log(error);
    }
  }, [setPetitions]);

  if (petitions) {
    return (
      <section className="dashboard-petition-container">
        <Title level={ 4 } className="petitions-title">Últimas Petições</Title>
        {petitions.map((peticao, index) => {
          const { titulo, tiposDeBeneficio, subtipo, dataDeCriacao, resumo } = peticao;

          const bothPartsDate = dataDeCriacao.split('T');
          const publicationDate = bothPartsDate[0].split('-');
          const time = bothPartsDate[1].split(':');

          const timeObject = {
            hours: time[0],
            minutes: time[1],
          };

          const dateObject = {
            year: publicationDate[0],
            month: publicationDate[1],
            day: publicationDate[2] };

          return (
            <Card key={ index } className="card-petition">
              {tiposDeBeneficio
                .map((beneficio, indexBeneficio) => (
                  <Tag
                    key={ indexBeneficio }
                    className="petition-tag"
                  >
                    {captalize(beneficio)}
                  </Tag>))}
              {subtipo.map((eachSubtipo, indexSubtipo) => (
                <Text
                  key={ indexSubtipo }
                  className="petition-subtitle"
                >
                  {captalize(eachSubtipo)}
                </Text>))}
              <Text className="petition-title">
                {titulo}
              </Text>
              <div className="third-line">
                <Text
                  className="publication-petition"
                >
                  {`Publicação:
                  ${dateObject.day}/${dateObject.month}/${dateObject.year} -
                  ${timeObject.hours}:${timeObject.minutes}`}
                </Text>
                <div className="preview-petition">
                  <EyeOutlined
                    className="petition-eye"
                    onClick={ () => resumeModal(resumo) }
                  />
                  <Text
                    className="preview-petition-text"
                    onClick={ () => resumeModal(resumo) }
                  >
                    Pré-visualizar petição
                  </Text>
                </div>
              </div>
            </Card>
          );
        })}
      </section>
    );
  } return null;
}
