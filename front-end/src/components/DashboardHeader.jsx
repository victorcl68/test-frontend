import React from 'react';

import { Image } from 'antd';

import previdenciaristaLogo from '../previdenciarista-logo.png';
import getProfilePicture from '../utils/DashboardHeaderFunctions';
import { profileModal } from './Modals';

import 'antd/dist/antd.min.css';
import '../styles/DashboardHeader.css';

export default function DashboardHeader() {
  const previdenciaLogo = 'previdenciarista-logo-dashboard';
  const profilePictureAlt = 'profile-picture';

  const profilePicture = getProfilePicture();
  return (
    <section className="dashboard-header-container">
      <Image
        className={ previdenciaLogo }
        preview={ false }
        src={ previdenciaristaLogo }
        alt={ previdenciaLogo }
      />
      <Image
        onClick={ profileModal }
        className="profile-picture"
        preview={ false }
        src={ profilePicture }
        alt={ profilePictureAlt }
      />
    </section>
  );
}
