import React, { useEffect, useState } from 'react';

import { Card, Typography } from 'antd';

import { getModulos } from '../utils/DashboardCardsFunctions';
import captalize from '../utils/DashboardCardsPetitionsFunctions';
import colorsArray from '../utils/DashboardTypeIconsFunctions';
import iconsArray from './TypesIcons';

import 'antd/dist/antd.min.css';
import '../styles/DashboardCards.css';

export default function DashboardCard() {
  const [modules, setModules] = useState([]);
  const { Text } = Typography;

  useEffect(() => {
    try {
      const getModulosToSet = async () => {
        const modulosResolved = await getModulos();
        setModules(modulosResolved);
      };
      getModulosToSet();
    } catch (error) {
      console.log(error);
    }
  }, []);

  if (modules) {
    return (
      <section className="dashboard-card-container">
        {modules.map((modulo, index) => {
          const { data } = modulo;
          const moduloData = data[0];

          const { tipo: string } = moduloData;

          const totalData = moduloData.total;
          const totalMonthly = moduloData.totalPeriodo.mensal;

          const typeCapitalized = captalize(string);

          return (
            <Card key={ index } className="card-types">
              <Text className="card-type">
                {iconsArray()[index]}
                {typeCapitalized}
              </Text>
              <Text
                style={ { color: colorsArray()[index] } }
                className="card-total"
              >
                {totalData}
              </Text>
              <Text className="card-total-monthly">
                {`Este mês: ${totalMonthly}`}
              </Text>
            </Card>
          );
        })}
      </section>
    );
  } return null;
}
