import React, { useContext, useEffect, useState } from 'react';

import 'antd/dist/antd.min.css';
import '../styles/DashboardPagination.css';

import { Pagination } from 'antd';

import AppContext from '../context/Context';
import getPetitions from '../utils/DashboardPetitionsFunctions';

export default function DashboardPagination() {
  const [totalPagination, setTotalPagination] = useState(0);
  const { setPetitions } = useContext(AppContext);

  const xTotalCount = 'x-total-count';

  useEffect(() => {
    const getInitialPetitionsFunction = (number) => {
      getPetitions(number)
        .then(({ headers }) => {
          const totalCount = parseInt((headers[xTotalCount] / 2), 10);
          const totalCountFormatted = parseInt((totalCount.toString() + 0), 10);
          setTotalPagination(totalCountFormatted);
        });
    };
    getInitialPetitionsFunction();
  }, []);

  const getPetitionsFunction = (number) => {
    getPetitions(number).then(({ data }) => { setPetitions(data); });
  };

  return (
    <section className="dashboard-pagination-container">
      <Pagination
        className="pagination-component"
        onChange={ getPetitionsFunction }
        total={ totalPagination }
        showSizeChanger={ false }
      />
    </section>
  );
}
