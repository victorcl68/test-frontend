import React, { useState } from 'react';

import PropTypes from 'prop-types';

import AppContext from './Context';

const Provider = ({ children }) => {
  const [petitions, setPetitions] = useState([]);
  const contextValue = {
    petitions,
    setPetitions,
  };

  return (
    <AppContext.Provider value={ contextValue }>
      {children}
    </AppContext.Provider>
  );
};

export default Provider;

Provider.propTypes = {
  children: PropTypes.shape({}).isRequired,
};
