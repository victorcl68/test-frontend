import captalize from '../utils/DashboardCardsPetitionsFunctions';
import { changePosition } from '../utils/DashboardCardsFunctions';

describe('Tests functions inside Dashboard Type Icons Functions file', () => {
  it('returns the right content inside an array',
    () => {
      const arrayIn = [2, 1, 2, 1];
      const arrayOut = [2, 2, 1, 1];

      expect(changePosition(arrayIn, 2, 1)).toStrictEqual(arrayOut);
    });
  it('returns the right string with an simple case',
    () => {
      const stringIn = 'PALAVRA';
      const stringOut = 'Palavra';

      expect(captalize(stringIn)).toStrictEqual(stringOut);
    });
  it('returns the right string with both different cases',
    () => {
      const stringInPetitions = 'PETICOES';
      const stringOutPetitions = 'Petições';
      const stringInCalculations = 'CALCULOS';
      const stringOutCalculations = 'Cálculos';

      expect(captalize(stringInPetitions)).toStrictEqual(stringOutPetitions);
      expect(captalize(stringInCalculations)).toStrictEqual(stringOutCalculations);
    });
});
