import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import Login from '../pages/Login';

describe('Tests if the elements in Login page are visible', () => {
  it('renders an image with the right alt text',
    () => {
      render(
        <MemoryRouter>
          <Login />
        </MemoryRouter>,
      );

      const previdenciaristaAlt = 'previdenciarista-logo-login';
      expect(screen.getByAltText(previdenciaristaAlt)).toBeInTheDocument();
    });
  it('renders both texts labels for inputs',
    () => {
      render(
        <MemoryRouter>
          <Login />
        </MemoryRouter>,
      );

      const emailTextLabel = 'Email ou CPF';
      const passwordTextLabel = 'Senha';

      expect(screen.getByText(emailTextLabel)).toBeInTheDocument();
      expect(screen.getByText(passwordTextLabel)).toBeInTheDocument();
    });
  it('renders both inputs with the right placeholder',
    () => {
      render(
        <MemoryRouter>
          <Login />
        </MemoryRouter>,
      );

      const emailOrCpfPlaceholderText = 'Digite seu email ou CPF';
      const passwordPlaceholderText = 'Digite sua senha';

      expect(screen.getByPlaceholderText(emailOrCpfPlaceholderText)).toBeInTheDocument();
      expect(screen.getByPlaceholderText(passwordPlaceholderText)).toBeInTheDocument();
    });
  it('renders the right link to reset password',
    () => {
      render(
        <MemoryRouter>
          <Login />
        </MemoryRouter>,
      );

      const forgotPasswordLinkText = 'Esqueceu a senha?';
      expect(screen.getByRole('link')).toHaveTextContent(forgotPasswordLinkText);
    });
  it('renders a button with the right text inside',
    () => {
      render(
        <MemoryRouter>
          <Login />
        </MemoryRouter>,
      );

      const buttonText = 'Entrar';
      expect(screen.getByRole('button')).toHaveTextContent(buttonText);
    });
});
