import DashboardTypeIconsFunctions from '../utils/DashboardTypeIconsFunctions';

describe('Tests functions inside Dashboard Type Icons Functions file', () => {
  it('returns the right content inside an array',
    () => {
      const responseArray = ['blue', 'orange', 'green', 'purple'];
      expect(DashboardTypeIconsFunctions()).toStrictEqual(responseArray);
    });
});
