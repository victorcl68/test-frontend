/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import LoginBox from '../components/LoginBox';

export default function Login() {
  const navigate = useNavigate();
  const dashboard = '/dashboard';
  const { token } = localStorage;
  useEffect(() => {
    if (token) {
      navigate(dashboard);
    }
  }, [navigate, token]);

  if (!token) return <LoginBox />;
  return null;
}
