import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import DashboardHeader from '../components/DashboardHeader';
import DashboardCards from '../components/DashboardCards';
import DashboardPetitions from '../components/DashboardPetitions';
import DashboardPagination from '../components/DashboardPagination';

import 'antd/dist/antd.min.css';
import '../styles/Dashboard.css';

export default function Dashboard() {
  const navigate = useNavigate();
  const login = '/login';
  const { token } = localStorage;
  useEffect(() => {
    if (!token) {
      navigate(login);
    }
  }, [navigate, token]);
  if (!token) return null;
  return (
    <main className="main-dashboard-container">
      <DashboardHeader />
      <DashboardCards />
      <DashboardPetitions />
      <DashboardPagination />
    </main>
  );
}
